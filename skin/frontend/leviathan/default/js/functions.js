jQuery(function() {
    jQuery.each(jQuery('#leviathan-homepage-slider'), function() {
        jQuery('#leviathan-homepage-slider #slider').nivoSlider({
        	 effect: homepageSliderOptions['effect'],
             slices: homepageSliderOptions['slices'],
             boxCols: homepageSliderOptions['boxCols'],
             boxRows: homepageSliderOptions['boxRows'],
             animSpeed: homepageSliderOptions['animSpeed'],
             pauseTime: homepageSliderOptions['pauseTime'],
             startSlide: homepageSliderOptions['startSlide'],
             directionNav: homepageSliderOptions['directionNav'],
             directionNavHide: homepageSliderOptions['directionNavHide'],
             controlNav: homepageSliderOptions['controlNav'],
             controlNavThumbs: homepageSliderOptions['controlNavThumbs'],
             pauseOnHover: homepageSliderOptions['pauseOnHover'],
             manualAdvance: homepageSliderOptions['manualAdvance'],
             prevText: homepageSliderOptions['prevText'],
             nextText: homepageSliderOptions['nextText'],
             randomStart: homepageSliderOptions['randomStart'],
             beforeChange: function(){},
             afterChange: function(){},
             slideshowEnd: function(){},
             lastSlide: function(){},
             afterLoad: function(){}
	     });
    });
});