<?php

class Leviathan_LeviathanSlideshow_Model_Source_Fx
{
    public function toOptionArray()
    {
        return array(
			array('value' => 'fade',			'label' => Mage::helper('leviathanslideshow')->__('Fade')),
			array('value' => 'sliceDown',			'label' => Mage::helper('leviathanslideshow')->__('sliceDown')),
			array('value' => 'sliceDownLeft',		'label' => Mage::helper('leviathanslideshow')->__('sliceDownLeft')),
			array('value' => 'sliceUp',		'label' => Mage::helper('leviathanslideshow')->__('sliceUp')),
			array('value' => 'sliceUpLeft',		'label' => Mage::helper('leviathanslideshow')->__('sliceUpLeft')),
			array('value' => 'sliceUpDown',			'label' => Mage::helper('leviathanslideshow')->__('sliceUpDown')),
			array('value' => 'sliceUpDownLeft',			'label' => Mage::helper('leviathanslideshow')->__('sliceUpDownLeft')),
			array('value' => 'fold',			'label' => Mage::helper('leviathanslideshow')->__('fold')),
			array('value' => 'random',			'label' => Mage::helper('leviathanslideshow')->__('random')),
			array('value' => 'slideInRight',			'label' => Mage::helper('leviathanslideshow')->__('slideInRight')),
			array('value' => 'slideInLeft',			'label' => Mage::helper('leviathanslideshow')->__('slideInLeft')),
			array('value' => 'boxRandom',			'label' => Mage::helper('leviathanslideshow')->__('boxRandom')),
            array('value' => 'boxRain',		'label' => Mage::helper('leviathanslideshow')->__('boxRain')),
            array('value' => 'boxRainReverse',		'label' => Mage::helper('leviathanslideshow')->__('boxRainReverse')),
			array('value' => 'boxRainGrow',		'label' => Mage::helper('leviathanslideshow')->__('boxRainGrow')),
			array('value' => 'boxRainGrowReverse',		'label' => Mage::helper('leviathanslideshow')->__('boxRainGrowReverse')),
			
        );
    }
}
