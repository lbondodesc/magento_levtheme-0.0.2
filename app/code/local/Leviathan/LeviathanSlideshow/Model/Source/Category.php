<?php
class Leviathan_LeviathanSlideshow_Model_Source_Category
{
    public function toOptionArray()
    {
     	$options = array();
    	foreach ($this->getCategories() as $category) {
    		$options[] = array('value' => $category->getId(), 'label'=> $category->getName());
    	}
        return $options;
	}
	
	public function getCategories()
	{
		$category = Mage::getModel('catalog/category');
		$tree = $category->getTreeModel();
		$tree->load();
		
		$ids = $tree->getCollection()->getAllIds();
		
		$arr = array();
		
		if ( $ids ){
			foreach ($ids as $id){
				$cat = Mage::getModel('catalog/category');
				$cat->load($id);
				array_push($arr, $cat);
			}
		}
		
		return $arr; 
	}
	
}