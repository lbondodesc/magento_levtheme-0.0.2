<?php

class Leviathan_LeviathanSlideshow_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getCfg($optionString)
    {
        return Mage::getStoreConfig('leviathanslideshow/' . $optionString);
    }
	
	public function getCfgGeneral($optionString)
    {
        return Mage::getStoreConfig('leviathanslideshow/general/' . $optionString);
    }
    
	public function resizeImg($url, $width, $height)
    {
    	
    	$fileName = str_replace(Mage::getUrl('media'), Mage::getBaseDir ( 'media' ) . DS, $url );
    	$fileName = str_replace('/', DS, $fileName);
    	
        if (!is_file($fileName) || !is_readable($fileName)) {
            die(' ');
            return false;
        }
        $targetDir = dirname($fileName);
        $image = Varien_Image_Adapter::factory('GD2');
        $image->open($fileName);
        //$width = $this->getConfigData('resize_width');
        //$height = $this->getConfigData('resize_height');
        $image->keepAspectRatio(true);
        $image->resize($width, $height);
        $dest = $targetDir . DS . 'w' . $width . 'h' . $height . pathinfo($fileName, PATHINFO_BASENAME);
        $image->save($dest);

        return Mage::getBaseUrl('media') . str_replace(DS, '/', substr($dest, strlen(Mage::getBaseDir('media')) + 1));
    }
    
}
