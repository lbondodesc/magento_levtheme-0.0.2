<?php

class Leviathan_LeviathanSlideshow_Block_Slideshow extends Mage_Core_Block_Template
{
	/*
	protected function _construct()
	{
		parent::_construct();
		$this->setTemplate('leviathan/leviathanslideshow/slideshow.phtml');
	}
	*/
	
	
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	
	public function getCategoryProducts( $categoryId )
    {
        if ( empty($categoryId) ) return NULL;
        $category = Mage::getModel('catalog/category')->load(
                        intval($categoryId)
                    );
        $layer = Mage::getSingleton('catalog/layer');
        $layer->setCurrentCategory($category);
        $collection = $category->getProductCollection();
        $collection->addAttributeToSelect( array('name', 'url', 'small_image') );
        return $collection;
    }
    
    public function getSlideshowOptions()
    {
    	$options = Mage::helper('leviathanslideshow')->getCfg('slider_options');
    	return $options;
    }
    
    public function getSliderWidth()
    {
    	$width = Mage::helper('leviathanslideshow')->getCfg('slider_options/slideshow_width');
    	return (int)(empty($width)) ? 400 : $width;
    }
    
	public function getSliderHeight()
    {
    	$height = Mage::helper('leviathanslideshow')->getCfg('slider_options/slideshow_height');
    	return (int)(empty($height)) ? 200 : $height;
    }
}