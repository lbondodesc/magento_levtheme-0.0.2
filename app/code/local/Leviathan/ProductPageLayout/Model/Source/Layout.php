<?php

class Leviathan_ProductPageLayout_Model_Source_Layout
{
    public function toOptionArray()
    {
        return array(
			array('value' => '',			        'label' => Mage::helper('productpagelayout')->__('None')),
			array('value' => 'one_column',		    'label' => Mage::helper('productpagelayout')->__('1 column')),
			array('value' => 'two_columns_left',	'label' => Mage::helper('productpagelayout')->__('2 columns with left bar')),
			array('value' => 'two_columns_right',	'label' => Mage::helper('productpagelayout')->__('2 columns with right bar')),
			array('value' => 'three_columns',		'label' => Mage::helper('productpagelayout')->__('3 columns'))
        );
    }
}
