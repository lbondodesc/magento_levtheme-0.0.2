<?php

class Leviathan_ProductPageLayout_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLE   = 'themeadminoptions/productpagelayout/enabled';
    const XML_PATH_ONLY     = 'themeadminoptions/productpagelayout/only';
    const XML_PATH_LAYOUT   = 'themeadminoptions/productpagelayout/layout';

    public function isEnable()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE);
    }

    public function isUseOnlyDefault()
    {
        return Mage::getStoreConfig(self::XML_PATH_ONLY);
    }

    public function getDefaultLayout()
    {
        return Mage::getStoreConfig(self::XML_PATH_LAYOUT);
    }
}
