<?php
class Leviathan_CloudZoom_Block_Product_View_Media extends Mage_Catalog_Block_Product_View_Media
{
	
    protected function _beforeToHtml()
	{

        if (Mage::getStoreConfig('cloudzoom_section/general/enabled')) { 
            $this->setTemplate('leviathan/cloudzoom/catalog/product/view/media.phtml');
        }
        
        return $this;
    }
    
    public function getCloudConfig()
    {
    	$configOptions = Mage::getStoreConfig('cloudzoom_section/general');
    	$cloudOptions = array (
    		'zoomWidth'    . ':' . ($configOptions['zoom_width'] ? $configOptions['zoom_width'] : 250),	
    		'zoomHeight'   . ':' . ($configOptions['zoom_width'] ? $configOptions['zoom_width'] : 250),
	    	'position'     . ':' . "'" . $configOptions['position'] . "'",
	    	'adjustX'      . ':' . ($configOptions['adjust_x'] ? $configOptions['adjust_x'] : 0),
	    	'adjustY'      . ':' . ($configOptions['adjust_y'] ? $configOptions['adjust_y'] : 0),
	    	'tint'  	   . ':' . ($configOptions['tint_color'] ? 'true' : 'false'),
	    	'tintOpacity'  . ':' . ($configOptions['tint_opacity'] ? $configOptions['tint_opacity'] : 0.5),
	    	'lensOpacity'  . ':' . ($configOptions['lens_opacity'] ? $configOptions['lens_opacity'] : 0.5),
	    	'softFocus'    . ':' . ($configOptions['soft_focus'] ? 'true' : 'false'),
	    	'smoothMove'   . ':' . ($configOptions['smooth_move'] ? $configOptions['smooth_move'] : 3),
	    	'showTitle'    . ':' . ($configOptions['show_title'] ? 'true' : 'false'),
    		'titleOpacity' . ':' . ($configOptions['title_opacity'] ? $configOptions['title_opacity'] : 0.5)
    	);
    	$cloudOptions = implode(',', $cloudOptions);
    	return $cloudOptions;
    }
}
