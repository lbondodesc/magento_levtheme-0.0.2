<?php

class Leviathan_CloudZoom_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getCfgGeneral($optionString)
	{
        return Mage::getStoreConfig('cloudzoom_section/general/' . $optionString);
    }
    
     /*
     * Resizer for product images 
     */
	public function getImgUrl($t, $prod, $w, $h, $imgVersion='image', $f = NULL)
	{
		$imgUrl = '';
		if ($h <= 0) {
			$imgUrl = $t->helper('catalog/image')->init($prod, $imgVersion, $f)
				->constrainOnly(TRUE)
				->keepAspectRatio(TRUE)
				->keepFrame(FALSE)
				->resize($w);
		} else $imgUrl = $t->helper('catalog/image')->init($prod, $imgVersion, $f)->resize($w, $h);
	
		return $imgUrl;
	}
	
}
