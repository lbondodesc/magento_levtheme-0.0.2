<?php
class Leviathan_ThemeAdminOptions_Model_Source_Product_Displaytype
{
	
	public function toOptionArray()
    {
    	
    	return array(
			array('value' => 0, 'label' => Mage::helper('themeadminoptions')->__('Regular List')),
            array('value' => 1, 'label' => Mage::helper('themeadminoptions')->__('Carousel'))
        );
        
    }
   	
}