<?php
class Leviathan_ThemeAdminOptions_Model_Source_Font_Googlefonts
{
	
	const GOOGLE_FONTS_URL = 'http://fonts.googleapis.com/css?family=*';
    
	public function toOptionArray()
    {
    	
    	$options = array();
    	foreach ($this->leviathanGetGoogleFonts() as $font) {
    		$options[] = array('value' => $font, 'label'=> $font);
    	}
        return $options;
        
    }
    
	function leviathanGetGoogleFonts() 
	{
		static $list;
		if (is_null($list)) {
			$contentCss = file_get_contents(self::GOOGLE_FONTS_URL);
		
		
		$flag = preg_match_all("#font\-family: \'([^\']*)\'#", $contentCss, $matches);
		
		if ($flag) {
			 $list = array_combine($matches[1], $matches[1]);
		} else {
			 $list = array();
		}
		
		}
		return $list;
	}
}
