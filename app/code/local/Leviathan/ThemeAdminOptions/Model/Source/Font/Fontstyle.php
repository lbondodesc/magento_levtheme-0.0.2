<?php
class Leviathan_ThemeAdminOptions_Model_Source_Font_Fontstyle
{
	
	public function toOptionArray()
    {
    	
    	return array(
			array('value' => 'normal', 'label' => Mage::helper('themeadminoptions')->__('Normal')),
            array('value' => 'italic', 'label' => Mage::helper('themeadminoptions')->__('Italic')),
			array('value' => 'oblique', 'label' => Mage::helper('themeadminoptions')->__('Oblique'))
			
        );
        
    }
   	
}