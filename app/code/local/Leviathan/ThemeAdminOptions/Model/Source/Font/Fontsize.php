<?php
class Leviathan_ThemeAdminOptions_Model_Source_Font_Fontsize
{
	
	public function toOptionArray()
    {
    	
    	$options = array();
    	$i = 0;
    	for ($i = 9; $i<=70; $i++) {
    		$options[] = array('value' => $i, 'label'=> $i . 'px');
    	}
        return $options;
        
    }
   	
}