<?php

class Leviathan_ThemeAdminOptions_Model_ThemeAdminOptions extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('themeadminoptions/themeadminoptions');
    }
}